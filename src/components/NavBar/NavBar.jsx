import React from 'react';
import { Nav, Navbar } from 'react-bootstrap';

import { Link } from "react-router-dom";

export default function NavBar(){
    return (
    <Navbar bg= "dark" variant= "dark">
        <Nav className= "me-auto" >
            <Nav.Link><Link to ="/"> Hero </Link></Nav.Link>
            <Nav.Link><Link to ="/experience"> Experience </Link></Nav.Link>
            <Nav.Link><Link to ="/education"> Education </Link></Nav.Link>
            <Nav.Link><Link to ="/about"> About </Link></Nav.Link>
        </Nav>
    </Navbar>
    )
}