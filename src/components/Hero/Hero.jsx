import React from "react";
import { Table } from "react-bootstrap";



export default function HeroPage({hero}){
    return (
        <>
    <Table striped>
        <thead>
            <tr>
                <th><img src="{hero.name}" alt="David" /></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <h2>{hero.name} {hero.surname}</h2>
            </tr>
            
            <tr>
                <p>{hero.city}</p>
            </tr>
            <tr>
                <p>🗓️{hero.birthDate}</p>
            </tr>
            <tr>
                <p>
                📧 <a href={"email:" + hero.email}>david.delpozo@bootcamp-upgrade.com</a>
                </p>
            </tr>
            <tr>
                <p>📱 {hero.phone}</p>
            </tr>
            <tr>
                <p>
                <a href="{hero.gitLab}">My GitLab</a>
                </p>
            </tr>       
        </tbody>  
    </Table>
    </>
    )
}