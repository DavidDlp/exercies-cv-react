import React, {useContext} from "react";
import { DataContext} from '../../App'

export default function EducationPage(){
    const {educationData} = useContext(DataContext)
    console.log("CV", educationData)

    return (
    <div className ="container">
        <h1>Education</h1>
        <div>
            {educationData.map(item => {
                return (
                    <div key={JSON.stringify(item)}>
                        <h3>{item.name}</h3>
                        <p>{item.date}</p>
                        <p>{item.where}</p>
                    </div>
                )
            })}
        </div>
    </div>
    )
}