import React, {useState} from 'react';
import { Route, BrowserRouter, Routes } from "react-router-dom";
import './App.css';
import ExpiriencePage from "./components/Expirience/Expirience";
import HeroPage from "./components/Hero/Hero";
import EducationPage from "./components/Education/Education";
import AboutPage from "./components/About/About";
import NavBar from "./components/NavBar/NavBar";
import {CV} from "./CV/CV";
import Footer from './components/Footer/Footer';

export const DataContext = React.createContext(null)

const {hero, education, experience, languages, aboutMe} = CV

function App() {

  const [educationData, setEducationData ] = useState(education)
  const [experienceData, setExperienceDate] = useState(experience) 
  
  return (
    <DataContext.Provider value = {{educationData, experienceData, setExperienceData, setEducationData}}>
    <BrowserRouter>
      <div className= 'container'>
        <header>
          <NavBar/>
        </header>
        <Routes>
          <Route path="/">
            <Route index element={ <HeroPage hero= {hero}></HeroPage>} />
            <Route path="expirience" element={ <ExpiriencePage experience= {experience}></ExpiriencePage>} />
            <Route path="education" element={ <EducationPage/>} />
            <Route path="about" element={ <AboutPage languages= {languages} aboutMe= {aboutMe}></AboutPage>} />
          </Route>
        </Routes>
        <footer>
          <Footer/>
        </footer>  
      </div>
    </BrowserRouter>
    </DataContext.Provider>
  );
}

export default App;
