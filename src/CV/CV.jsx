export const CV = {
    hero: {
        name: "David",
        surname: "Del pozo Figueroa",
        city: "Madrid",
        email: "david.delpozo@bootcamp-upgrade.com",
        birthDate: "Algun día de 1985",
        phone: "(+34) 66666666",
        image: "",
        gitLab: "",
        
    },

    experience: [
        {
            name: "Buzo profesional",
            date: "2021",
            where: "Avramar",
            description: "Diferentes trabajos subacuaticos"
        },
        {
            name: "Practicas-Estudiante",
            date: "2020",
            where: "Lanzarate",
            description: "Montaje de acuarios, cuidado de instalaciones de acuicultura y sus seres vivos, investigacion y desarrollo microalgas."
        },
        {
            name: "Vigilante de Seguridad",
            date: "2018",
            where: "Prosegur",
            description: "Control de camaras y accesos"
        }
    ],

    education: [
        {
            name: "Programmer",
            date: "2021",
            where: "Upgrade-hub",
        },
        {
            name: " Grade in Acuiculture",
            date: "2020",
            where: "maritime fishing school ",
        },
        {
            name: "Profesional dive",
            date: "2021",
            where: "maritime fishing school",
        }
    ],

    languages: [
        {
            language: "English",
            wrlevel: "Basic",
            splevel: "Basic"
        },
        {
            language: "Italian",
            wrlevel: "middle",
            splevel: "middle"
        },
    ],

    aboutMe: [
        {info: "agfdgdf"},
        {info: "gfgfg"},
        {info: "fgfdgad"},
    ]


}